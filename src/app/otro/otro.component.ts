import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-otro',
  templateUrl: './otro.component.html',
  styleUrls: ['./otro.component.scss']
})
export class OtroComponent implements OnInit {

  temporal: string;
  constructor() { }

  ngOnInit() {
  }

  interceptar($event){
    this.temporal = $event;
  }

}
