import { Component, OnInit } from '@angular/core';
import { Materia } from '../models/materia';

import { MateriasService } from '../services/materias.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  materias: Materia[];
  placeholder: string = "No mas dulces para duque";
  evilTitle = '<h1>Hello</h1>';

  constructor(private ms: MateriasService) { }

  ngOnInit() {
    this.ms.getMaterias()
    .subscribe(
      materias => this.materias = materias.json(),
      err => console.log(err),
      () => {
        //Repasando Scope
          console.log(this);
        this.materias.forEach(()=>{console.log(this)});
      }
    );
  }

  clickMe(e){
    console.log(e);
  }

  alert(v){
    alert(v);
  }

}
