import { Component, OnInit, Output, EventEmitter, SimpleChanges } from '@angular/core';


@Component({
  selector: 'app-escritor',
  templateUrl: './escritor.component.html',
  styleUrls: ['./escritor.component.scss']
})
export class EscritorComponent implements OnInit {

  @Output() emisor = new EventEmitter<string>();

  texto: string;

  constructor() { }

  ngOnInit() {

  }

  emit(){
    this.emisor.emit(this.texto);
  }
}
